import time, utils
from kafka.errors import NoBrokersAvailable

TOPIC_NAME = 'iot_heartbeat'

producer = utils.get_producer()
print(repr(producer))

j = 0
while True:
    print("\nGenerating packet ->", j+1)
    data = utils.get_data('okai_scooter')
    producer.send(TOPIC_NAME, value=data)
    print(data)
    j += 1
    time.sleep(utils.get_random_sleep_time())