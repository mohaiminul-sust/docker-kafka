import time, utils

TOPIC_NAME = 'iot_heartbeat'
producer = utils.get_producer()

j = 0
while True:
    print("\nGenerating packet ->", j+1)
    data = utils.get_data('jimi_scooter')
    print(data)
    producer.send(TOPIC_NAME, value=data)
    j += 1
    time.sleep(utils.get_random_sleep_time())