import time, utils

consumer = utils.get_consumer('iot_heartbeat')
print(repr(consumer))

if __name__ == "__main__":
    for event in consumer:
        event_data = event.value
        print(event_data)
        time.sleep(0.1)