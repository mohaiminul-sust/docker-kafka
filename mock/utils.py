from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import NoBrokersAvailable

from random import randint, choice
import json, time, uuid

KAFKA_URL = '13.229.65.188:9092'

IMEI_OPTIONS = {
    'OKAI_SCOOTER': ['864431042443780', '862506042784507', '862506042790900', ],
    'JIMI_SCOOTER': ['355951093180854', '355951093177413', '355951092997456', '355951093048578', '355951093177215', ]
}


def get_random_imei_for(iot_type: str) -> str:
    try:
        options = IMEI_OPTIONS[iot_type.upper()]
    except Exception as ex:
        options = ['1000000000', ] # bug catcher imei

    return str(choice(options))


def get_random_sleep_time() -> int:
    return randint(2, 5)


def get_data(iot_type: str) -> dict:
    return {
        'id': str(uuid.uuid4()),
        'iot_type': iot_type.upper(),
        'imei': get_random_imei_for(iot_type),
        'is_locked': bool(randint(0, 1)),
        'is_charging': bool(randint(0, 1)),
        'battery_pct': randint(25, 99),
        'pos_accuracy': randint(75, 82),
        'timestamp': int(time.time())
    }


def get_producer() -> KafkaProducer:
    try:
        producer = KafkaProducer(
            bootstrap_servers=[KAFKA_URL],
            value_serializer=lambda x: json.dumps(x).encode('utf-8')
        )
        return producer

    except NoBrokersAvailable:
        print("Broker not found...retrying in 5s")
        time.sleep(5)
        return get_producer()


def get_consumer(topic: str) -> KafkaConsumer:
    return KafkaConsumer(
        topic,
        bootstrap_servers=[KAFKA_URL],
        auto_offset_reset='latest',#'earliest',
        enable_auto_commit=True,
        group_id='iot_feed',
        value_deserializer=lambda x: json.loads(x.decode('utf-8'))
    )